## Provision runhyve webapp

Clone this repository and run installation script:
`sudo ./install.sh`
The script will install Chef and apply Cookbook on current host

After it's complete you can run tests:
`sudo ./test.sh`

## Configuration
Customization can be make in `webapp.json` file.

More information: https://gitlab.com/runhyve/webapp
