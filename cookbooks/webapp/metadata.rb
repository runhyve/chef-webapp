name 'webapp'
maintainer 'Mateusz Kwiatkowski'
maintainer_email 'mateusz@runateam.com'
license 'All Rights Reserved'
description 'Configures Runhyve webapp'
long_description 'Configures Runhyve webapp'
version '0.0.1'
chef_version '>= 14.7' if respond_to?(:chef_version)

issues_url 'https://gitlab.com/runhyve/chef-webapp/issues'
source_url 'https://gitlab.com/runhyve/chef-webapp/master'

supports 'freebsd'
